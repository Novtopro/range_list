# RangeList

This tiny project implements a range list. It is a list of ranges, where each range is a pair of integers. To see how it works, please refer to the RSpec tests.

To run the RSpec tests, run the following command:

1. `bundle`
2. `bundle exec rspec`