class CustomRange
  attr_reader :min, :max

  class << self
    def from_arr(arr)
      raise 'The array must contain exactly two elements' if arr.size != 2

      new(arr[0], arr[1])
    end
  end

  def initialize(min, max)
    @min = min
    @max = max
  end

  def valid?
    !min.nil? && !max.nil? && max >= min
  end

  def empty?
    min == max
  end

  def to_s
    "[#{min}, #{max})"
  end

  def overlapped_with?(range)
    max >= range.min && min <= range.max
  end

  def merge(*ranges)
    ranges = ranges.flatten
    return self if ranges.empty?

    new_min = [ranges.map(&:min).min, min].min
    new_max = [ranges.map(&:max).max, max].max

    CustomRange.new(new_min, new_max)
  end

  def split(range)
    return [self] unless overlapped_with?(range)

    splited_ranges = split_overlapped_range(range)
    splited_ranges.select { |it| !it&.empty? }
  end

  private

  def split_overlapped_range(range)
    if range.min <= min
      range.max >= max ? [] : [CustomRange.new(range.max, max)]
    elsif range.max >= max
      [CustomRange.new(min, range.min)]
    else
      [CustomRange.new(min, range.min), CustomRange.new(range.max, max)]
    end
  end
end