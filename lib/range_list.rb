# frozen_string_literal: true

require_relative './custom_range'

class RangeList
  attr_reader :list

  def initialize
    @list = []
  end

  def add(range_arr)
    range = to_range(range_arr)
    raise 'Invalid range' unless range.valid?

    return self if range.empty?

    ranges_before     = list.select { |it| it.max < range.min }
    ranges_overlapped = list.select { |it| it.overlapped_with?(range) }
    ranges_after      = list.select { |it| it.min > range.max }

    merged_range = range.merge(*ranges_overlapped)
    @list =  ranges_before + [merged_range] + ranges_after

    self
  end

  def remove(range_arr)
    range = to_range(range_arr)
    raise 'Invalid range' unless range.valid?

    return self if range.empty?

    @list = list.map { |it| it.split(range) }.flatten

    self
  end

  def print
    puts to_s
  end

  def to_s
    list.map { |it| it.to_s }.join(' ')
  end

  private

  def to_range(range_expr)
    case range_expr
    when CustomRange then range_expr
    when Array       then CustomRange.from_arr(range_expr)
    when Range       then CustomRange.new(range_expr.begin, range_expr.end)
    else
      raise 'Invalid range'
    end
  end
end
