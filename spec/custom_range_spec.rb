# frozen_string_literal: true

require_relative '../lib/custom_range'
require 'pry'

describe CustomRange do
  it '#valid?' do
    range = described_class.new(1, 5)
    expect(range).to be_valid

    range = described_class.new(5, 1)
    expect(range).not_to be_valid
  end
end