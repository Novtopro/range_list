# frozen_string_literal: true

require_relative '../lib/range_list'
require 'pry'

describe RangeList do
  let(:subject) { described_class.new }

  it 'works' do
    subject.add([1, 5])
    expect(subject.to_s).to eq '[1, 5)'

    subject.add([10, 20])
    expect(subject.to_s).to eq '[1, 5) [10, 20)'

    subject.add([20, 20])
    expect(subject.to_s).to eq '[1, 5) [10, 20)'

    subject.add([20, 21])
    expect(subject.to_s).to eq '[1, 5) [10, 21)'

    subject.add([2, 4])
    expect(subject.to_s).to eq '[1, 5) [10, 21)'

    subject.add([3, 8])
    expect(subject.to_s).to eq '[1, 8) [10, 21)'

    subject.remove([10, 10])
    expect(subject.to_s).to eq '[1, 8) [10, 21)'

    subject.remove([10, 11])
    expect(subject.to_s).to eq '[1, 8) [11, 21)'

    subject.remove([15, 17])
    expect(subject.to_s).to eq '[1, 8) [11, 15) [17, 21)'

    subject.remove([3, 19])
    expect(subject.to_s).to eq '[1, 3) [19, 21)'
  end
end
